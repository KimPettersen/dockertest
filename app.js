var koa = require('koa');
var sendfile = require('koa-sendfile');
var app = koa();

app.use(function* (next) {
  console.log('Hittting the function')
  var stats = yield* sendfile.call(this, __dirname + '/index.html');
  if (!this.status) this.throw(404)
})

app.use(function* (next) {
  this.body = 'YES!!!'
});


app.listen(3000);

console.log('Listening on port 3000');
